import React, {Component} from 'react';

export default class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nid: props.data.nid,
            title: props.data.title,
            text: props.data.body,
        };
    }
    render() {
        const {title, text} = this.state;
        return (
            <div>
                <h1>{title}</h1>
                <p>{text}</p>
            </div>

        )
    }
}
