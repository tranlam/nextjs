import React, {Component} from 'react';
import ReactPaginate from 'react-paginate';
import Link from 'next/link';

export default class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            offset: 0,
            data: props.data,
            perPage: 10,
            currentPage: 0
        };

        this.handlePageClick = this
            .handlePageClick
            .bind(this);

        this.receivedData = this
            .receivedData
            .bind(this);
    }
    receivedData() {
        const data = this.state.data;
        const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
        const postData = slice.map(p => {
            return (
                <div key={p.node.id}>
                    <Link href="/blog/[slug]" as={`/blog/${p.node.id}`}>
                        <a><p>{p.node.title}</p></a>
                    </Link>
                </div>)
        })

        this.setState({
            pageCount: Math.ceil(data.length / this.state.perPage),
            postData
        })
    }
    handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;

        this.setState({
            currentPage: selectedPage,
            offset: offset
        }, () => {
            this.receivedData()
        });

    };

    componentDidMount() {
        this.receivedData()
    }
    render() {
        return (
            <div>
                {this.state.postData}
                <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
            </div>

        )
    }
}
